package br.edu.up.playermp3;

import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity
    implements MediaPlayer.OnCompletionListener, View.OnTouchListener {

  private MediaPlayer player;
  private ImageButton btnTocar;
  private ImageButton btnAtivar;
  private ImageView capa;
  private SeekBar barra;
  private int musicaAtual = 5;
  private boolean tocarAleatorio = false;

  private int[] musicas = {
      R.raw.anitta_bang,
      R.raw.jota_quest_na_moral,
      R.raw.pink_floyd_another_brick_in_the_wall_p2,
      R.raw.sambo_sunday_bloody_sunday,
      R.raw.talking_heads_psycho_killer,
      R.raw.the_doors_light_my_fire,
      R.raw.zeca_baleiro_disritmia
  };

  private int[] capas = {
      R.drawable.anitta,
      R.drawable.jota_quest,
      R.drawable.pink_floyd,
      R.drawable.sambo,
      R.drawable.talking_heads,
      R.drawable.the_doors,
      R.drawable.zeca_baleiro
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    btnTocar = (ImageButton) findViewById(R.id.btnTocar);
    btnAtivar = (ImageButton) findViewById(R.id.btnAtivar);
    capa = (ImageView) findViewById(R.id.capa);
    barra = (SeekBar) findViewById(R.id.seekBar);
    barra.setOnTouchListener(this);
  }

  public void onClickTocar(View v){

    if (player == null) {
      tocar();
    } else if (!player.isPlaying()){
      btnTocar.setImageResource(R.drawable.pausar50px);
      player.start();
    } else {
      btnTocar.setImageResource(R.drawable.tocar50px);
      player.pause();
    }
  }

  private void tocar() {

    btnTocar.setImageResource(R.drawable.pausar50px);

    TextView txt = (TextView) findViewById(R.id.txtCONTEUDO);
    MediaMetadataRetriever mmr = new MediaMetadataRetriever();

    String path = "android.resource://" + getPackageName() + "/" + R.raw.anitta_bang;

    Log.d("TESTE", path);
    Uri uri = Uri.parse(path);

    mmr.setDataSource(this,uri);

    String album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
    txt.setText(album);

    int idCapa = capas[musicaAtual];
    capa.setImageResource(idCapa);

    int idMusica = musicas[musicaAtual];
    player = MediaPlayer.create(this, idMusica);
    player.setOnCompletionListener(this);

    int max = player.getDuration();
    barra.setMax(max);

    player.start();

    atualizarBarra();
  }

  private void atualizarBarra() {

    if (player != null && player.isPlaying()) {

      barra.setProgress(player.getCurrentPosition());

      Runnable processo = new Runnable() {
        public void run() {
          atualizarBarra();
        }
      };

      new Handler().postDelayed(processo, 1000);
    }
  }

  public void onClickParar(View v){
    limparPlayer();
  }

  private void limparPlayer() {
    if (player != null && player.isPlaying()){
      btnTocar.setImageResource(R.drawable.tocar50px);
      player.stop();
      player.release();
      player = null;
    }
  }

  public void onClickAvancar(View v){
    limparPlayer();
    selecionarMusica(1);
    tocar();
  }

  public void onClickVoltar(View v){
    limparPlayer();
    selecionarMusica(-1);
    tocar();
  }

  private void selecionarMusica(int incremento) {

    if (tocarAleatorio){
      Random r = new Random();
      musicaAtual = r.nextInt(musicas.length -1);
    } else {
      musicaAtual = musicaAtual + incremento;
      if (musicaAtual > musicas.length -1){
        musicaAtual = 0;
      } else if (musicaAtual < 0){
        musicaAtual = musicas.length -1;
      }
    }
  }

  public void onClickAtivar(View v){
    if (tocarAleatorio){
      tocarAleatorio = false;
      btnAtivar.setImageResource(R.drawable.sequencial50px);
    } else {
      tocarAleatorio = true;
      btnAtivar.setImageResource(R.drawable.aleatorio50px);
    }
  }

  @Override
  public void onCompletion(MediaPlayer mediaPlayer) {
    selecionarMusica(1);
    tocar();
  }

  @Override
  public boolean onTouch(View view, MotionEvent motionEvent) {
    int progresso = barra.getProgress();
    player.seekTo(progresso);
    return false;
  }
}